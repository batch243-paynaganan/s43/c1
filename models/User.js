const mongoose=require('mongoose')

const userSchema=new mongoose.Schema({
    firstName: {type:String,required:[true,'pamystery effect yarn?']},
    lastName: {type:String,required:[true,'wala ka family name beh?']},
    email: {type:String,required:[true,'so pano ka maglolog in?']},
    password: {type:String,required:[true,'open source account yarn?']},
    isAdmin: {type:Boolean,default: false},
    mobileNo: {type:String,required:[true, 'di naman kami scammer bhie']},
    cartQuantity:{type: Number},
    items:[{
        productId:{type:String,require:[true,'Product Id is required']},
        addedOn:{type: Date, default: new Date()}
    }],
})

module.exports=mongoose.model('User',userSchema)